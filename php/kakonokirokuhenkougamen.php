<?php
 session_start();

if(isset($_POST['modoru'])){
    header('Location: kakonokirokugamen.php');
 }
 if(isset($_POST['modoru1'])){
    header('Location: kakonokirokugamen.php');
 }
if(isset($_POST['save'])){
    $diary=htmlspecialchars($_POST['diary'], ENT_QUOTES, 'UTF-8');
    $zip1 = mb_convert_kana($_POST['taion'], 'a', 'UTF-8');
    $zip2 = mb_convert_kana($_POST['weight'], 'a', 'UTF-8');
    $zip3 = mb_convert_kana($_POST['fat'], 'a', 'UTF-8');
    $zip4 = mb_convert_kana($_POST['sleephour'], 'a', 'UTF-8');
    $zip5 = mb_convert_kana($_POST['sleepminute'], 'a', 'UTF-8');
    $zip6 = mb_convert_kana($_POST['motionhour'], 'a', 'UTF-8');
    $zip7 = mb_convert_kana($_POST['motionminute'], 'a', 'UTF-8');
    $zip8 = mb_convert_kana($_POST['cigarette'], 'a', 'UTF-8');
    $zip9 = mb_convert_kana($_POST['steps'], 'a', 'UTF-8');
    $zip10 = mb_convert_kana($_POST['b_top'], 'a', 'UTF-8');
    $zip11 = mb_convert_kana($_POST['b_under'], 'a', 'UTF-8');
    if (preg_match("/^[0-9.]+$/", $_POST['taion'])&&preg_match("/\A\d{2}[.]\d{1}\z/", $zip1)||empty($_POST['taion'])) {
        if (preg_match("/^[0-9.]+$/", $_POST['weight'])&&(preg_match("/\A\d{3}[.]\d{1}\z/", $zip2)||preg_match("/\A\d{2}[.]\d{1}\z/", $zip2))||empty($_POST['weight'])) {
            if (preg_match("/^[0-9.]+$/", $_POST['fat'])&&(preg_match("/\A\d{2}[.]\d{1}\z/", $zip2)||preg_match("/\A\d{1}[.]\d{1}\z/", $zip2))||empty($_POST['fat'])) {
                if (preg_match("/^[0-9]+$/", $_POST['sleephour'])&&strlen($zip4)<3||empty($_POST['sleephour'])) {
                    if (preg_match("/^[0-9]+$/", $_POST['sleepminute'])&&strlen($zip5)<3||empty($_POST['sleepminute'])) {
                        if (preg_match("/^[0-9]+$/", $_POST['motionhour'])&&strlen($zip6)<3||empty($_POST['motionhour'])) {
                            if (preg_match("/^[0-9]+$/", $_POST['motionminute'])&&strlen($zip7)<3||empty($_POST['motionminute'])) {
                                if (preg_match("/^[0-9]+$/", $_POST['cigarette'])&&strlen($zip8)<4||empty($_POST['cigarette'])) {
                                    if (preg_match("/^[0-9]+$/", $_POST['steps'])&&strlen($zip9)<6||empty($_POST['steps'])) {
                                        if (preg_match("/^[0-9]+$/", $_POST['b_top'])&&strlen($zip10)<4||empty($_POST['b_top'])) {
                                            if (preg_match("/^[0-9]+$/", $_POST['b_under'])&&strlen($zip11)<4||empty($_POST['b_under'])) {
                                                try{
                                                    if($_POST['sleepminute']==0){
                                                        $sleepmi="00";
                                                    }elseif(($_POST['sleepminute']>0)&&($_POST['sleepminute']<10)){
                                                        $sleepmi="0".$_POST['sleepminute'];
                                                    }elseif(($_POST['sleepminute']>=10)&&($_POST['sleepminute']<60)){
                                                        $sleepmi=$_POST['sleepminute'];
                                                    }else{
                                                        $sleepmi="00";
                                                    }
                                                    $sleep = $_POST['sleephour'].$sleepmi."00";//20000
                                
                                                    if($_POST['motionminute']==0){
                                                        $motionmi="00";
                                                    }elseif(($_POST['motionminute']>0)&&($_POST['motionminute']<10)){
                                                        $motionmii="0".$_POST['motionminute'];
                                                    }elseif(($_POST['motionminute']>=10)&&($_POST['motionminute']<60)){
                                                        $motionmi=$_POST['motionminute'];
                                                    }else{
                                                        $motionmi="00";
                                                    }
                                                    $motion = $_POST['motionhour'].$motionmi."00";//20000
                                                    //$db = new PDO('mysql:host=localhost; dbname=kaihatu','green','ivy');
                                                    $db = new PDO('mysql:host=172.16.3.22; dbname=kaihatu','green','ivy');
                                                    $sql1 = "update recording_main set 
                                                            Calorie_asa=?,
                                                            Calorie_hiru=?,
                                                            Calorie_yoru=?,
                                                            steps=?,
                                                            motion=?,
                                                            sleep=?,
                                                            Taion=? 
                                                            where id=? and hiduke=? ;";
                                                    $stmt = $db->prepare($sql1);
                                                    $stmt->execute(array($_POST['morning'],$_POST['lunch'],$_POST['dinner'],$_POST['steps'],$motion,$sleep,$_POST['taion'],$_SESSION["ID"],$_SESSION["date"] ));
                                
                                                    $sql1 = "update recording_second set 
                                                            weight=?,
                                                            cigarette=?,
                                                            diary=?,
                                                            blood_top=?,
                                                            blood_under=?,
                                                            fat=?,
                                                            water_ml=?,
                                                            alcohol_ml=?
                                                            where id=? and hiduke=? ;";
                                
                                                    $stmt = $db->prepare($sql1);
                                                    $stmt->execute(array($_POST['weight'],$_POST['cigarette'],$_POST['diary'],$_POST['b_top'],$_POST['b_under'],$_POST['fat'],$_POST['water'],$_POST['alcohol'],$_SESSION["ID"],$_SESSION["date"] ));
                                                    header('Location: meinmenyugamen.php');
                                                }catch(\Throwable $th){
                                
                                                }
                                            }else{
                                                echo "<script type='text/javascript'>alert('血圧は3桁以下の整数で入力してください');</script>";
                                            }
                                        }else{
                                            echo "<script type='text/javascript'>alert('血圧は3桁以下の整数で入力してください');</script>";
                                        }
                                    }else{
                                        echo "<script type='text/javascript'>alert('歩数は5桁以下の整数で入力してください');</script>";
                                    }
                                }else{
                                    echo "<script type='text/javascript'>alert('喫煙本数は3桁以下の整数で入力してください');</script>";
                                } 
                            }else{
                               echo "<script type='text/javascript'>alert('運動時間は2桁以下の整数で入力してください');</script>";
                            }        
                        }else{
                            echo "<script type='text/javascript'>alert('運動時間は2桁以下の整数で入力してください');</script>";
                        }  
                    }else{
                        echo "<script type='text/javascript'>alert('睡眠時間は2桁以下の整数で入力してください');</script>";
                    }     
                }else{
                    echo "<script type='text/javascript'>alert('睡眠時間は2桁以下の整数で入力してください');</script>";
                }
            }else{
                echo "<script type='text/javascript'>alert('体脂肪率は00.0または0.0の形式で入力してください');</script>";
            }
        }else{
            echo "<script type='text/javascript'>alert('体重は00.0または000.0の形式で入力してください');</script>";
        }
    }else{
        echo "<script type='text/javascript'>alert('体温は00.0の形式で入力してください');</script>";
    }   
}
$taion="";
$steps="";
$sleeph="";
$sleeps="";
try {
    //$db = new PDO('mysql:host=localhost; dbname=kaihatu','green','ivy');
    $db = new PDO('mysql:host=172.16.3.22; dbname=kaihatu','green','ivy');
    $sql1 = "select * from recording_main where id=? and hiduke=? ;";
    $stmt = $db->prepare($sql1);
    $stmt->execute(array($_SESSION["ID"],$_SESSION["date"] ));
    $result = $stmt->fetch();
    $morning=$result[2];
    $lunch=$result[3];
    $dinner=$result[4];
    $steps=$result[5];
    $motionh=mb_substr( $result[6],0,2);
    $motions=mb_substr( $result[6],3,2);
    $sleeph=mb_substr( $result[7],0,2);
    $sleeps=mb_substr( $result[7],3,2);
    $taion=$result[8];
    if($steps==0){
        $steps="";
    }
    if($taion==0){
        $taion="";
    }

    $arraym=array(
        1=>    '<option value="1">食べない</option>',
        2=>    '<option value="2">1～100未満</option>',
        3=>    '<option value="3">100～200未満</option>',
        4=>    '<option value="4">200～300未満</option>',
        5=>    '<option value="5">300～400未満</option>',
        6=>    '<option value="6">400～500未満</option>',
        7=>    '<option value="7">500～600未満</option>',
        8=>    '<option value="8">600～700未満</option>',
        9=>    '<option value="9">700～800未満</option>',
        10=>   '<option value="10">800～900未満</option>',
        11=>   '<option value="11">900～1000以上</option>',                    
    );
    $arrayl=array(
        1=>    '<option value="1">食べない</option>',
        2=>    '<option value="2">1～100未満</option>',
        3=>    '<option value="3">100～200未満</option>',
        4=>    '<option value="4">200～300未満</option>',
        5=>    '<option value="5">300～400未満</option>',
        6=>    '<option value="6">400～500未満</option>',
        7=>    '<option value="7">500～600未満</option>',
        8=>    '<option value="8">600～700未満</option>',
        9=>    '<option value="9">700～800未満</option>',
        10=>   '<option value="10">800～900未満</option>',
        11=>   '<option value="11">900～1000以上</option>',            
    );
    $arrayd=array(
        1=>    '<option value="1">食べない</option>',
        2=>    '<option value="2">1～100未満</option>',
        3=>    '<option value="3">100～200未満</option>',
        4=>    '<option value="4">200～300未満</option>',
        5=>    '<option value="5">300～400未満</option>',
        6=>    '<option value="6">400～500未満</option>',
        7=>    '<option value="7">500～600未満</option>',
        8=>    '<option value="8">600～700未満</option>',
        9=>    '<option value="9">700～800未満</option>',
        10=>   '<option value="10">800～900未満</option>',
        11=>   '<option value="11">900～1000以上</option>',
    );
} catch (\Throwable $th) {
        //throw $th;
}

$space=" ";
$url="kakonokirokugamen.php";
    echo "
        <h1>健康記録帳</h1>
        <hr><br>
        <center>
        <main>
            <button type='button' class= 'modoru1' name='modoru1' onclick='modoru()'>記録せずに<br>戻る</button>
            <p class='kakonokiroku'>過去の記録</p>
            <u class='nentukihi'>".$_SESSION["date"]."</u>
            <form action='' method='POST'>
            <input type='submit' class= 'kiroku1' value='記録する' style='background-color: aqua;' name='save'><br>
            
            <u class='kihonkoumoku'>基本項目</u>
            <p class='taionmozi'>体温<input type='text'  maxlength='4' oninput='value = value.replace(/[^0-9.]+/i,'".$space."');'  class='taion' value='".$taion."' name='taion'><a class='do'>℃</a></p>
            <p class='hosuumozi'>歩数<input type='text'  maxlength='5' oninput='value = value.replace(/[^0-9]+/i,'".$space."');'  class='hosuu' value='".$steps."' name='steps'><a class='ho'>歩</a></p>
            <p class='suiminmozi'>睡眠時間
            <input type='text' class='zi' maxlength='2' oninput='value = value.replace(/[^0-9]+/i,'".$space."');'  value='".$sleeph."' name='sleephour'>
            <a class='zimozi'>時間</a>
            <input type='text' class='hun' maxlength='2' oninput='value = value.replace(/[^0-9]+/i,'".$space."');'   value='".$sleeps."' name='sleepminute'>
            <a class='hunmozi'>分</a></p>
                <p class='karorimozi'>摂取カロリー
                    <a class='asamozi'>朝</a>
                    <select class='asa' name='morning'>";

    $arraym[$morning]="<option value='$morning' selected>".$_SESSION["asa"]."</option>";
    foreach($arraym as $arraym_value){
        echo $arraym_value;
    }
                   
    echo        "</select>
                <a class='asakcal'>kcal</a>
                <a class='hirumozi'>昼</a>
                <select class='hiru' name='lunch'>";
    $arrayl[$lunch]="<option value='$lunch' selected>".$_SESSION["hiru"]."</option>";
    foreach($arrayl as $arrayl_value){
        echo $arrayl_value;
    }
    echo       "</select>
                <a class='hirukcal'>kcal</a>
            </p>
            <a class='banmozi'>晩</a>
            <select class='ban' name='dinner'>";
    $arrayd[$dinner]="<option value='$dinner' selected>".$_SESSION['ban']."</option>";
    foreach($arrayd as $arrayd_value){
        echo $arrayd_value;
    }
    echo   "</select>
            <a class='bankcal'>kcal</a>
            <p class='undoumozi'>運動時間
            <input type='text' class='zi2' maxlength='2' oninput='value = value.replace(/[^0-9]+/i,'".$space."');'  name='motionhour' value='".$motionh."'>
            <a class='zimozi2'>時間</a>
            <input type='text' class='hun2' maxlength='2' oninput='value = value.replace(/[^0-9]+/i,'".$space."');'  name='motionminute' value='".$motions."'>
            <a class='hunmozi2'>分</a></p>
            </p>";



    $db = new PDO('mysql:host=172.16.3.22; dbname=kaihatu','green','ivy');
    $sql1 = "select * from recording_second where id=? and hiduke=? ;";
    $stmt = $db->prepare($sql1);
    $stmt->execute(array($_SESSION["ID"],$_SESSION["date"] ));
    $result = $stmt->fetch();

    $weight=$result[3];
    $cigarette=$result[4];
    $diary=$result[5];
    $blood_top=$result[6];
    $blood_under=$result[7];
    $fat=$result[8];
    $water=$result[9];
    $alcohol=$result[10];
    if($weight==0){
        $weight="";
    }
    if($cigarette==0){
        $cigarette="";
    }
    if($blood_top==0){
        $blood_top="";
    }
    if($blood_under==0){
        $blood_under="";
    }
    if($fat==0){
        $fat="";
    }
    

    $array1=array(
        0=>    '<option value="0"></option>',
        1=>    '<option value="1">2000ml未満</option>',
        2=>    '<option value="2">2000～2200ml未満</option>',
        3=>    '<option value="3">2200～2400ml未満</option>',
        4=>    '<option value="4">2400～2600ml未満</option>',
        5=>    '<option value="5">2600～2800ml未満</option>',
        6=>    '<option value="6">2800～3000ml未満</option>',
        7=>    '<option value="7">3000ml以上</option>',                        
    );
    switch ($water) {
        case 0:
            $w= "";
            break;
        case 1:
            $w= "2000ml未満";
            break;
        case 2:
            $w= "2000～2200ml未満";
            break;
        case 3:    
            $w= "2200～2400ml未満";
            break;
        case 4:
            $w= "2400～2600ml未満";
            break;
        case 5:
            $w= "2600～2800ml未満";
            break;
        case 6:
            $w= "2800～3000ml未満";
            break;
        case 7:
            $w= "3000ml以上";
            break;
        }
    $array1[$water]="<option value='$water' selected>".$w."</option>";

    $array2=array(

        0=>    '<option value="0"></option>',
        1=>    '<option value="1">500ml未満</option>',
        2=>    '<option value="2">500ml以上</option>',
        3=>    '<option value="3">600ml以上</option>',
        4=>    '<option value="4">700ml以上</option>',
        5=>    '<option value="5">800ml以上</option>',
        6=>    '<option value="6">900ml以上</option>',
        7=>    '<option value="7">1000ml以上</option>',                      
    );
    switch ($water) {
        case 0:
            $al= "";
            break;
        case 1:
            $al= "500ml未満";
            break;
        case 2:
            $al= "500ml以上";
            break;
        case 3:
            $al= "600ml以上";
            break;
        case 4:
            $al= "700ml以上";
            break;
        case 5:
            $al= "800ml以上";
            break;
        case 6:
            $al= "900ml以上";
            break;
        case 7:
            $al= "1000ml以上";
            break;
        }
    $array2[$alcohol]="<option value='.$alcohol.' selected>".$al."</option>";

    echo       "<u class='sonotamozi'>その他の項目</u>
                <section class='sonota'>
                <p class='taijuumozi'>体重<input type='text'  maxlength='5' oninput='value = value.replace(/[^0-9.]+/i,'".$space."');' class='taijuu' value='".$weight."' name='weight'><a class='kg'>kg</a></p>
                <p class='ketuatumozi'>血圧<a class='ue'>上</a><input type='text' maxlength='3' oninput='value = value.replace(/[^0-9]+/i,'".$space."');'  class='ketuatu1' value='".$blood_top."' name='b_top'></p>
                <a class='sita'>下</a><input type='text' maxlength='3' oninput='value = value.replace(/[^0-9]+/i,'".$space."');'  class='ketuatu2' value='".$blood_under."' name='b_under'></p>
                <p class='taisiboumozi'>体脂肪率<input type='text'  maxlength='4' oninput='value = value.replace(/[^0-9.]+/i,'".$space."');' class='taisibou' value='".$fat."' name='fat'><a class='pa'>%</a></p>
                <p class='suibunmozi'>水分補給
                    <select class='suibun' name='water'>";
                    foreach($array1 as $array1_value){
                        echo $array1_value;
                    }
    echo           "</select>
                    <a class='ml'>ml</a>
                    <p class='insyumozi'>飲酒
                        <select class='insyu' name='alcohol'>";
                    foreach($array2 as $array2_value){
                        echo $array2_value;
                    }
    echo               "</select>
                        <a class='ml2'>ml</a>
                        <p class='kituenmozi'>喫煙<input type='text' maxlength='3' oninput='value = value.replace(/[^0-9]+/i,'".$space."');'  class='kituen' value='".$cigarette."' name='cigarette'><a class='hon'>本</a></p>
                        <p class='nikkimozi'>日記<input type='text' class='nikki' name='diary' value=".$diary.">
                        </p>
            </section>
            <button type='submit' class= 'modoru2' name='modoru' onclick='location.href = '".$url."';'>記録せずに<br>戻る</button>
            <input type='submit' class= 'kiroku2' value='記録する' style='background-color: aqua;' name='save'><br>
                </form>";
?>
<!DOCTYPE html>
 <html lang='ja'>
 <head>
 <meta charset='UTF-8'>
 <title></title>
 <meta name='viewport' content='width=device-width, initial-scale=1'>
 <link href='../css/kakonokirokuhenkougamen.css' rel='stylesheet' media='all'>
 <script type="text/javascript">
    window.onorientationchange = function () {
        switch ( window.orientation ) {
            case 0:
                
            case 90:
                alert('画面を縦にしてください');
                break;
            case -90:
                alert('画面を縦にしてください');
                break;
        }
    }
 </script>
 </head>
 <body>
            
           
        </main>
    </center>
    <script type="text/javascript">
        
        function modoru(){
          document.location.href = "kakonokirokugamen.php";
        }
    </script>    

</body>
</html>
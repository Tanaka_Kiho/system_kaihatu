<?php
 if(isset($_POST['send'])) {
  if(empty($_POST['ID'])){
    echo "<script type='text/javascript'>alert('未入力の項目があります');</script>";
  } 
  else if(empty($_POST['address'])){
    echo "<script type='text/javascript'>alert('未入力の項目があります');</script>";
  }
  else{
    header('Location:php/mailsend.php');
  }
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<link href="css/pasuwadosyokikagamen.css" rel="stylesheet" media="all">

<script type="text/javascript">
  window.onorientationchange = function () {
   switch ( window.orientation ) {
    case 0:
     break;
    case 90:
     alert('画面を縦にしてください');
     break;
    case -90:
     alert('画面を縦にしてください');
     break;
   }
  }
  </script>
  
</head>
<body>
    <h1>健康記録帳</h1>
    <hr><br>
    <center>
    <main>
    <u>パスワード初期化</u>
    <p class="setumei">登録されているメールアドレスに<br>
        IDと仮パスワードを送信します。<br>
        仮パスワードでログイン後に<br>
        設定のユーザー情報の変更から<br>
        パスワードを変更してください。<br>
    </p>
    <form action=" " method="POST">
    <p class="meru">メールアドレス　　<input type="email" class ="text" name="address"></p>
    <p class="id">ユーザーID　　<input type="text" class ="text2" name="ID"></p>
    <button type="button" class= "modoru" onclick="modoru()">ログインへ<br>戻る</button>
    <input type="submit" class= "sousin" value="メールを送信" style="background-color:blue;" name="send">
  </form>
    <script type="text/javascript">
        
        function modoru(){
          document.location.href = "PHP/roguinngamen.php";
        }
        </script>
    
</main>
</center>
</body>
</html>
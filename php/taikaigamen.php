<?php
session_start();

echo $_SESSION["ID"];
echo $_SESSION["name"];
?>


<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<link href="../css/taikaigamen.css" rel="stylesheet" media="all">

<script type="text/javascript">
window.onorientationchange = function () {
 switch ( window.orientation ) {
  case 0:
   break;
  case 90:
   alert('画面を縦にしてください');
   break;
  case -90:
   alert('画面を縦にしてください');
   break;
 }
}
</script>

</head>
<body>
    <h1>健康記録帳</h1>
    <hr><br>
    <center>
    <main>
    <form action="" method="post">
    <p class="keikoku">退会するためデータを削除します</p>
    <p class="setumei">ユーザー情報を削除するとこれまで記録<br>
        された情報は全て削除されます。<br>
        削除された情報は二度と戻りません。<br>
    </p>
    <p class="saidokeikoku">それでも本当に削除しますか？</p>
    <br>
    <input type="button" class= "modoru" value="戻る" style="background-color:green;" onclick="modoru()">
    <input type="button" class= "sakujo" value="削除" style="background-color:purple;" onclick="taikai()">
    </form>

</main>
</center>
<script type="text/javascript"> 
    function modoru(){
        document.location.href = "henkoutaikaisentaku.php"
    }
    function taikai(){
      if(window.confirm("退会しますか？")){
        <?php
          if (isset($_SESSION["ID"])) {
            //$errorMessage = "ログアウトしました。";
        } else {
            //$errorMessage = "セッションがタイムアウトしました。";
        }
        
        ?>
        document.location.href="taikaiyou.php";
      }else{

      }
    }

</script>
</body>
</html>
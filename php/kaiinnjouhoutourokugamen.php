<?php
session_start();

//echo $_SESSION["name"];
if(isset($_POST['kaiintouroku'])) {
    $name=htmlspecialchars($_POST['namae'], ENT_QUOTES, 'UTF-8');
    $tall=mb_convert_kana($_POST['sin'], 'a', 'UTF-8');
    $wei=mb_convert_kana($_POST['tai'], 'a', 'UTF-8');

    //未入力チェック
    if(empty($_POST['namae'])){
        echo "<script type='text/javascript'>alert('未入力の項目があります');</script>";
    }else if(empty($_POST['sin'])){
        echo "<script type='text/javascript'>alert('未入力の項目があります');</script>";
    }else if(empty($_POST['tai'])){
        echo "<script type='text/javascript'>alert('未入力の項目があります');</script>";
    }else if (strlen($name) > 15) {
        echo "<script type='text/javascript'>alert('ニックネームは15文字以下で入力してください');</script>";
    }else{
        if (preg_match("/\A\d{3}[.]\d{1}\z/", $wei)||preg_match("/\A\d{2}[.]\d{1}\z/", $wei)) {
            if (preg_match("/\A\d{3}[.]\d{1}\z/", $tall)) {
                try{
                    
                    //$dbh = new PDO('mysql:host=localhost; dbname=kaihatu','green','ivy');
                    
                    $dbh = new PDO('mysql:host=172.16.3.22; dbname=kaihatu','green','ivy');
                    
                    //
                    
                
                    $stmt = $dbh->prepare("INSERT INTO user(id,mail,pass) VALUES(?,?,?)");
                    $stmt->bindParam(1, $_SESSION['ID']);
                    $stmt->bindParam(2, $_SESSION['mail']);
                    $stmt->bindParam(3, $_SESSION['pass']);
                    $stmt->execute();
              
        
                    $b=$_SESSION['ID'];
                    $stmt = $dbh->prepare("UPDATE user SET name = ?,tall = ?,weight = ? WHERE id='$b'");
                    $stmt->bindParam(1, $name);
                    $stmt->bindParam(2, $tall);
                    $stmt->bindParam(3, $wei);
                    $stmt->execute();
                    
                    $today=date('Y-m-d');
                    $stmt = $dbh->prepare("INSERT INTO recording_second(id,hiduke,`weight`,tall) VALUES('$b','$today',?,?)");
                    $stmt->bindParam(1, $wei);
                    $stmt->bindParam(2, $tall);
                    $stmt->execute();
                
                    $_SESSION['name']=$name;
                    $_SESSION['tall']=$tall;
                    $_SESSION['weight']=$wei;
                    header('Location:meinmenyugamen.php');}
                    catch(PDOEception $e){
                      echo $e->getMessage();
                      exit;
                    }
                }else{
                    echo "<script type='text/javascript'>alert('身長は000.0の形式で入力してください');</script>";
                }
            }else{
                echo "<script type='text/javascript'>alert('体重は00.0または000.0の形式で入力してください');</script>";
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<link href="../css/kaiinnjouhoutourokugamen.css" rel="stylesheet" media="all">
<!--<link href="css/PC/kaiinnjouhoutourokugamen.css" rel="stylesheet" media="all">-->

<script type="text/javascript">
window.onorientationchange = function () {
 switch ( window.orientation ) {
  case 0:
   break;
  case 90:
   alert('画面を縦にしてください');
   break;
  case -90:
   alert('画面を縦にしてください');
   break;
 }
}
</script>

</head>
<body>
    <h1>健康記録帳</h1>
    <hr><br>
    <button type="button" class= "modoru" onclick="modoru()">トップ画面へ<br>戻る</button>
    <center>
    <main>
    <form action="kaiinnjouhoutourokugamen.php" method="post">
    <u class="kaiinn">会員登録</u>
    <p class="nemukoumoku">ニックネーム<input type="text" class ="nemu" name ="namae"><a class=hissu1>必須</a></p>
    <!--
    <p class="umarekoumoku">生年月日<input type="text" class ="umare"><a class="nenmozi">年</a><a class=hissu2>必須</a></p>
    <p class="koumoku"><input type="text" class ="tuki"><a class="tukimozi">月</a><input type="text" class ="niti"><a class="nitimozi">日</a></p>
    -->
    <p class="sintyoukoumoku">身長<input type="text" maxlength="5" oninput="value = value.replace(/[^0-9.]+/i,'');" class ="sintyou" name ="sin"><a class="cm">cm</a><a class=hissu3>必須</a></p>
    <p class="taijuukoumoku">体重<input type="text" maxlength="5" oninput="value = value.replace(/[^0-9.]+/i,'');" class ="taijuu" name ="tai"><a class="kg">kg</a><a class=hissu4>必須</a></p>
    <!--
    <p class="seibetukoumoku">性別
    <select class="seibetu">
        <option value=""></option>
        <option value="男性">男性</option>
        <option value="女性">女性</option>
        <option value="その他">その他</option>
    </select>
    <a class=ninni3>任意</a>
    </p>
    <p class="keikoku">上記で登録された情報と「本日の記録」で<br>
        記録された情報を個人情報が特定できない状態で<br>
        統計に利用してもよろしいですか？<br>
        統計の集計対象になります。<br>
        同意されない場合当サイトをご利用いただけません。<br>
    </p>
    <input type="checkbox" class="check"><a class="doui">上記の内容に同意します</a><br>
-->
    <input type="submit" class= "touroku" value="会員登録" style="background-color: aqua;" name="kaiintouroku">
    </form>
</main>
</center>
<script type="text/javascript">
    
    function modoru(){
      document.location.href = "../toppugamen.html";
    }
    </script>

</body>
</html>
<?php
session_start();
echo "   <h1>健康記録帳</h1>
<hr><br>
<button type='button' class= 'modoru' onclick='modoru()'>トップ画面へ<br>戻る</button>
<center>
<main>
<form action='sinnkitourokugamenn.php' method='post'>
<u class='sinnki'>新規登録</u>";

if(isset($_POST['sinkitouroku'])) {
  //html特殊文字処理
  $id=htmlspecialchars($_POST['id'], ENT_QUOTES, 'UTF-8');
  $address=htmlspecialchars($_POST['adoresu'], ENT_QUOTES, 'UTF-8');
  $pass=htmlspecialchars($_POST['pasu'], ENT_QUOTES, 'UTF-8');
  $kakunin=htmlspecialchars($_POST['kakuninn'], ENT_QUOTES, 'UTF-8');

  //パスワード英数字判定
  $num=preg_match("/[0-9]{1,}/", $pass);
  $char=preg_match("/[A-Za-z]{1,}/", $pass);
  
  //未入力チェック
  if(empty($_POST['id'])){
    echo "<script type='text/javascript'>alert('未入力の項目があります');</script>";
  } 
  else if(empty($_POST['adoresu'])){
    echo "<script type='text/javascript'>alert('未入力の項目があります');</script>";
  }
  else if(empty($_POST['pasu'])){
    echo "<script type='text/javascript'>alert('未入力の項目があります');</script>";
  }
  //メールアドレスチェック
  else if (strlen($address) > 50) {
    echo "<script type='text/javascript'>alert('メールアドレスは50文字以下で入力してください');</script>";
  }
  //IDチェック
  else if (strlen($id) > 15) {
    echo "<script type='text/javascript'>alert('IDは3文字以上15文字以下で入力してください');</script>";
  }elseif (strlen($id) < 3) {
    echo "<script type='text/javascript'>alert('IDは3文字以上15文字以下で入力してください');</script>";
  }
  //パスワードチェック
  else if (strlen($pass) > 20) {
    echo "<script type='text/javascript'>alert('パスワードは8文字以上20文字以下で入力してください');</script>";
  }elseif (strlen($pass) < 8) {
    echo "<script type='text/javascript'>alert('パスワードは8文字以上20文字以下で入力してください');</script>";
  }else if ($num==0) {
    echo "<script type='text/javascript'>alert('パスワードはアルファベットと数字どちらも使用してください');</script>";
  }else if($char==0){
    echo "<script type='text/javascript'>alert('パスワードはアルファベットと数字どちらも使用してください');</script>";
  }else if (strcmp($pass,$kakunin)!=0) {
    echo "<script type='text/javascript'>alert('パスワードが確認用と一致しません');</script>";
  }
  else{
    try{
      //$dbh = new PDO('mysql:host=localhost; dbname=kaihatu','green','ivy');
      $dbh = new PDO('mysql:host=172.16.3.22; dbname=kaihatu','green','ivy');
      $sql1 = 'select count(*) from user where id=?';
      $stmt = $dbh->prepare($sql1);
      $stmt->execute(array($id));
      $result = $stmt->fetch();
      $sql2 = 'select count(*) from user where mail=?';
      $stmt = $dbh->prepare($sql2);
      $stmt->execute(array($address));
      $result1 = $stmt->fetch();
      if ($result[0] != 0){
        echo "<br><br><a id='check'>入力されたIDは既に使用されています</a>";
      }
      else if ($result1[0] != 0){
        echo "<br><br><a id='check'>入力されたメールアドレスは既に使用されています</a>";
      }else{
        
      
        $_SESSION['ID']=$id;
        $_SESSION['mail']=$address;
        $_SESSION['pass']=$pass;
      
        header('Location:kaiinnjouhoutourokugamen.php');
      }
    }catch(PDOEception $e){
      echo $e->getMessage();
      exit;
    }  
  }        
}


?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<link href="../css/sinnkitourokugamenn.css" rel="stylesheet" media="all">
<!--<link href="css/PC/sinnkitourokugamenn.css" rel="stylesheet" media="all">-->
<script type="text/javascript">
  window.onorientationchange = function () {
   switch ( window.orientation ) {
    case 0:
     break;
    case 90:
     alert('画面を縦にしてください');
     break;
    case -90:
     alert('画面を縦にしてください');
     break;
   }
  }
  </script>
</head>
<body>
  

    <p class="adoresumozi">メールアドレス<input type="email" class = "adoresu" name ="adoresu"></p>
    <p class="idmozi">ユーザーID<input type="text" class = "id" name ="id"></p>
    <p class="keikoku">※IDは後で変更できません！</p>
    <p class="pasumozi">パスワード<input type="password" class = "pasu" name ="pasu"></p>
    <p class="kakuninnmozi">パスワード<br>確認<br><input type="password" class = "kakuninn" name ="kakuninn"></p><br>
    <input type="submit" class= "touroku" value="新規登録" style="background-color: aqua; " name="sinkitouroku"><br>
    </form>
    <a href="roguinngamen.php"class="rinku">既に登録済みの方はこちらから</a>
    <script type="text/javascript">
        function modoru(){
          document.location.href = "../toppugamen.html";
        }

        </script>
</main>
</center>
</body>
</html>
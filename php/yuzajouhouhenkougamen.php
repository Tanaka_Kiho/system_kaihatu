<?php
session_start();
/*
echo $_SESSION["ID"];
echo $_SESSION["name"];
echo $_SESSION["mail"];
echo $_SESSION["tall"];*/
//echo $_SESSION["weight"];

//html特殊文字処理
$name=htmlspecialchars($_POST['namae'], ENT_QUOTES, 'UTF-8');
$addr=htmlspecialchars($_POST['adoresu'], ENT_QUOTES, 'UTF-8');
$zip1=htmlspecialchars($_POST['sin'], ENT_QUOTES, 'UTF-8');
$kakunin=htmlspecialchars($_POST['kakuninn'], ENT_QUOTES, 'UTF-8');

if(isset($_POST['henkou'])){
  //未入力チェック
  if(empty($_POST['namae'])){
    echo "<script type='text/javascript'>alert('未入力の項目があります');</script>";
  } 
  else if(empty($_POST['adoresu'])){
    echo "<script type='text/javascript'>alert('未入力の項目があります');</script>";
  }
  else if(empty($_POST['sin'])){
    echo "<script type='text/javascript'>alert('未入力の項目があります');</script>";
  }
  //メールアドレスチェック
  else if (strlen($addr) > 50) {
    echo "<script type='text/javascript'>alert('メールアドレスは50文字以下で入力してください');</script>";
  }
  
  else if (strlen($name) > 15) {
    echo "<script type='text/javascript'>alert('ニックネームは15文字以下で入力してください');</script>";
  }
  if (preg_match("/\A\d{3}[.]\d{1}\z/", $zip1)||empty($_POST['sin'])) {
    echo "<script type='text/javascript'>alert('身長は000.0の形式で入力してください');</script>";
  }
  else{
    echo "<script type='text/javascript'>alert('変更を反映するには再度ログインが必要です。トップ画面に戻ります。');</script>";
    header("Location: henkouyou.php");
  }
}

?>


<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<link href="../css/yuzajouhouhenkougamen.css" rel="stylesheet" media="all">
<!--<link href="css/PC/yuzajouhouhenkougamen.css" rel="stylesheet" media="all">-->

<script type="text/javascript">
window.onorientationchange = function () {
 switch ( window.orientation ) {
  case 0:
   break;
  case 90:
   alert('画面を縦にしてください');
   break;
  case -90:
   alert('画面を縦にしてください');
   break;
 }
}
</script>

</head>
<body>
<h1>健康記録帳</h1>
    <hr><br>
    <center>
    <main>
    <form action=" " method="post">
    <u>ユーザー情報の変更</u>
    <p class="idkoumoku">ユーザーID<input type="text" class ="id" value="<?php echo $_SESSION["ID"]; ?>" style="background-color:#c0c0c0" readonly></p>
    <p class="nemukoumoku">ニックネーム<input type="text" class ="nemu" name ="namae" value="<?php echo $_SESSION["name"]; ?>"><a class="hissu1">必須</a></p>
    <p class="adoresukoumoku">メールアドレス<input type="email" class ="adoresu" name ="adoresu" value="<?php echo $_SESSION["mail"]; ?>"><a class="hissu2">必須</a></p>
    <!--<p class="umarekoumoku">生年月日<input type="text" class ="umare"><a class="nenmozi">年</a><a class="hissu2">必須</a></p>
    <input type="text" class ="tuki"><a class="tukimozi">月</a><input type="text" class ="hi"><a class="nitimozi">日</a></p>
    -->
    <p class="sintyoukoumoku">身長<input type="text" maxlength="5" oninput="value = value.replace(/[^0-9.]+/i,'');"  class ="sintyou" name ="sin" value="<?php echo $_SESSION["tall"]; ?>"><a class="cm">cm</a><a class="hissu3">必須</a></p>
    <!--<p class="taijuukoumoku">体重<input type="text" class ="taijuu" name ="tai" value="<?php echo $_SESSION["weight"]; ?>"><a class="kg">cm</a><a class="hissu4">必須</a></p>
-->
    <!--<p class="seibetukoumoku">性別
        <select class="seibetu">
            <option value=""></option>
            <option value="男性">男性</option>
            <option value="女性">女性</option>
            <option value="その他">その他</option>
        </select>
        <a class=ninni>任意</a>
        </p>
    -->
    <p class="koku">
      ※ユーザー情報を変更するには再ログインが必要なため、トップ画面へ戻ります。<br>
      変更ボタンを押す際は変更内容に間違いがないかを確認ください。
    </p>
    <button type="button" class= "modoru" onclick="modoru()">変更せずに<br>戻る</button>
    <input type="submit" class= "henkou" value="変更" style="background-color: orange;" name ="henkou" >
</form>
</main>
</center>
<script type="text/javascript">

    function modoru(){
      document.location.href = "yuzajouhouhennkousentakugamen.php";
    }
    
    </script>

</body>
</html>